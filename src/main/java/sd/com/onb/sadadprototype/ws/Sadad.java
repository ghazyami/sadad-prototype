package sd.com.onb.sadadprototype.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import sd.com.onb.sadadprototype.ws.model.QueryRequest;
import sd.com.onb.sadadprototype.ws.model.QueryResponse;

@WebService(serviceName = "Sadad")
public class Sadad {

    @WebMethod(operationName = "QueryInvoice")
    public QueryResponse Query(@WebParam(name = "QueryRequest") QueryRequest request) {
        return QueryResponse.Builder.sampleResponse();
    }
    
//    @WebMethod(operationName = "PayInvoice")
//    public PayResponse Pay(@WebParam(name = "PayRequest") PayRequest request) {
//        return PayResponse.Builder.sampleResponse();
//    }
}
